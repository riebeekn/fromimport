defmodule PhxGitlabCiCdWeb.Router do
  use PhxGitlabCiCdWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PhxGitlabCiCdWeb do
    pipe_through :browser

    get "/", ProductController, :index
    resources "/products", ProductController, except: [:index]
  end

  # Other scopes may use custom stacks.
  # scope "/api", PhxGitlabCiCdWeb do
  #   pipe_through :api
  # end
end
