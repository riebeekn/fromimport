defmodule PhxGitlabCiCd.Repo do
  use Ecto.Repo,
    otp_app: :phx_gitlab_ci_cd,
    adapter: Ecto.Adapters.Postgres
end
