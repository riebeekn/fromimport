# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :phx_gitlab_ci_cd,
  ecto_repos: [PhxGitlabCiCd.Repo]

# Configures the endpoint
config :phx_gitlab_ci_cd, PhxGitlabCiCdWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "OI2zhKyGB/8oH7HH3pQ46kWNCGVztQzpyJ6mRESsSckM1lpUx4B7pcAL6L/4w8vt",
  render_errors: [view: PhxGitlabCiCdWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PhxGitlabCiCd.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
